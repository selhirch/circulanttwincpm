W3 = "(A'+B'+E')(C'+D'+E')(A'+B'+C')(A+D+E+F')(A+B+C+H')(C+D+E'+J)(B+E+F+G')(A+B+H+I)(B+D+H+I')(B+C+D+E)(A+C'+D'+G')(C+D+E+F)(B+C+D'+I)(A'+C+E'+I')(B'+D'+E+I+J')(B'+D'+E+I'+J)(D+E+F+G)(A+B+E+H)(A'+B+D'+F'+G)(A+C+G+H')(B'+C+E'+G'+H)(A'+C'+D+H'+I)(A+C'+E'+F+J')(A+D+F'+J)(A+B'+D'+G+I'+J')(A+B'+D'+G+I+J)(A'+B'+D'+F'+G')(B'+E+F+G)(B'+C'+E'+G'+H')(A'+C'+D'+H'+I')(B'+C'+G+H)(C'+D'+H+I)(C+D+E+J')(A'+C'+E'+F'+J')(A'+E'+F+J)(D'+E'+H'+I'+J')(D'+E'+I+J)"

W4 = "(B+E)(C+E)(A+C)(A+D)(B+D)(D+E+F)(A+E+G)(A+B+H)(B+C+I)(C+D+J)(E+F+G)(A+G+H)(B+H+I)(D+F+J)(C+I+J)(A+B'+E'+G'+H')(A'+B+C'+H'+I')(B'+C+D'+I'+J')(C'+D+E'+F'+J')(A'+D'+E+F'+G')(F+G+H+I+J)(B'+D'+F+G+H+I'+J')(A'+B'+C'+D'+F+G+H'+I+J')(A'+C'+D'+E'+F+G'+H+I+J')(C'+E'+F'+G+H+I+J')(A'+C'+F+G+H'+I'+J)(A'+B'+C'+E'+F+G'+H+I'+J)(B'+C'+D'+E'+F'+G+H+I'+J)(B'+E'+F+G'+H'+I+J)(A'+B'+D'+E'+F'+G+H'+I+J)(A'+D'+F'+G'+H+I+J)(B'+D'+E'+F+G'+H'+I'+J')(A'+C'+E'+F'+G+H'+I'+J')(A'+B'+D'+F'+G'+H+I'+J')(B'+C'+E'+F'+G'+H'+I+J')(A'+C'+D'+F'+G'+H'+I'+J)"

W2 = "(E'+J)(D'+I)(C'+F')(B'+J')(A'+J')(B'+F')(C'+H)(A'+I')(A+B+C+D+E)(D'+H')(E'+I')(B'+G)(A'+F)(C'+G')(D'+G')(E'+H')"
def sp(X):
    Y = X.replace("(", "")
    Y = Y.split(")")
    return Y

def check(x, y):
    a = 0
    if x == y:
        a = 1
    elif x == y + str("'"):
        a = -1
    return a

def str_eq(X):
    A = [0]*10
    for x in X:
        A[0] |= check(x, 'A')
        A[1] |= check(x, 'B')
        A[2] |= check(x, 'C')
        A[3] |= check(x, 'D')
        A[4] |= check(x, 'E')
        A[5] |= check(x, 'F')
        A[6] |= check(x, 'G')
        A[7] |= check(x, 'H')
        A[8] |= check(x, 'I')
        A[9] |= check(x, 'J')
    return A

P = sp(W4)
for i in range(len(P)-1):
    temp = P[i].split("+")
    A = str_eq(temp)
    print("{}".format(A))

print(len(P)-1)
