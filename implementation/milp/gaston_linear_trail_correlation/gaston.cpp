#include "main.h"
#include <sstream>
#include <fstream>
#include <string>
string itos(int i) {stringstream s; s << i; return s.str(); }
GRBEnv env = GRBEnv();
#define N 320

// Gaston parameters
int TT[5] = {25,32,52,60,63} ;           // [t_0, t_1, t_2, t_3, t_4]
int EE[5] = {0, 60, 22, 27, 4} ;         // rho_east
int WW[5] = {0, 56, 31, 46, 43} ;        // rho_west
int RR = 1 ;                             // P <<< RR
int SS = 18 ;                            // Q <<< SS
int UU = 23 ;                            // E <<< UU

const int XOR2[4][4] = {{1, 1, -1, 0},
                        {1, -1, 1, 0},
                        {-1, 1, 1, 0},
                        {-1, -1, -1, 2}
};

void XOR2add(GRBModel& model, GRBVar& a, GRBVar& b, GRBVar& c ) {
    for(int i = 0; i<4; i++){
        GRBLinExpr tt = 0 ;
        tt  = XOR2[i][0] * a + XOR2[i][1] * b + XOR2[i][2] * c ;
        model.addConstr(tt >= -XOR2[i][3]) ;
    }
}

// Constraints for LAT entry 4
void sbox_w4(GRBModel& model, vector<GRBVar>& x, vector<GRBVar>& y, GRBVar z) {
    int i, j;
    int N4 = 89;
    for (i = 0; i < N4; i++) {
        GRBLinExpr tmp = 0;
        int sum = 0 ;
        for(j = 0; j<5; j++){
            tmp += W4[i][j]*x[4-j];           // 4, 3, 2, 1, 0
            if(W4[i][j] == -1){
                sum+=1;
            }
        }
        for(j = 5; j<10; j++){
            tmp += W4[i][j]*y[9-j];         // 9, 8, 7, 6, 5
            if(W4[i][j] == -1){
                sum+=1;
            }
        }
        model.addConstr(tmp - 10*z >= 1 - sum - 10 );
    }
}

// Constraints for LAT entry -4
void sbox_w44(GRBModel& model, vector<GRBVar>& x, vector<GRBVar>& y, GRBVar z) {
    int i, j;
    int N44  = 91;
    for (i = 0; i < N44; i++) {
        GRBLinExpr tmp = 0;
        int sum = 0 ;
        for(j = 0; j<5; j++){
            tmp += W44[i][j]*x[4-j];
            if(W44[i][j] == -1){
                sum+=1;
            }
        }
        for(j = 5; j<10; j++){
            tmp += W44[i][j]*y[9-j];
            if(W44[i][j] == -1){
                sum+=1;
            }
        }
        model.addConstr(tmp - 10*z >= 1 - sum - 10 );
    }
}

// Constraints for LAT entry 8
void sbox_w8(GRBModel& model, vector<GRBVar>& x, vector<GRBVar>& y, GRBVar z) {
    int i, j;
    int N8 = 29;
    for (i = 0; i < N8; i++) {
        GRBLinExpr tmp = 0;
        int sum = 0 ;
        for(j = 0; j<5; j++){
            tmp += W8[i][j]*x[4-j];
            if(W8[i][j] == -1){
                sum+=1;
            }
        }
        for(j = 5; j<10; j++){
            tmp += W8[i][j]*y[9-j];
            if(W8[i][j] == -1){
                sum+=1;
            }
        }
        model.addConstr(tmp - 10*z >= 1 - sum - 10 );
    }
}

// Constraints for LAT entry -8
void sbox_w88(GRBModel& model, vector<GRBVar>& x, vector<GRBVar>& y, GRBVar z) {
    int i, j;
    int N88 = 22;
    for (i = 0; i < N88; i++) {
        GRBLinExpr tmp = 0;
        int sum = 0 ;
        for(j = 0; j<5; j++){
            tmp += W88[i][j]*x[4-j];
            if(W88[i][j] == -1){
                sum+=1;
            }
        }
        for(j = 5; j<10; j++){
            tmp += W88[i][j]*y[9-j];
            if(W88[i][j] == -1){
                sum+=1;
            }
        }
        model.addConstr(tmp - 10*z >= 1 - sum - 10 );
    }
}


int gaston_model(int threadNumber, int rounds);
int gaston_model(int threadNumber, int rounds) {
    int i, j, r ;
    try {
	    env.set(GRB_IntParam_LogToConsole, 1);
		env.set(GRB_IntParam_Threads, threadNumber);
		GRBModel model = GRBModel(env);

		// env.set(GRB_IntParam_MIPFocus, 3);

		// Variables
		vector<vector<GRBVar>> X(rounds, vector<GRBVar>(320));
		vector<vector<GRBVar>> Y(rounds, vector<GRBVar>(320));
		vector<vector<GRBVar>> D(rounds, vector<GRBVar>(64)) ;

		vector<vector<GRBVar>> w4(rounds, vector<GRBVar>(64)) ;
		vector<vector<GRBVar>> w44(rounds, vector<GRBVar>(64)) ;
		vector<vector<GRBVar>> w8(rounds, vector<GRBVar>(64)) ;
		vector<vector<GRBVar>> w88(rounds, vector<GRBVar>(64)) ;
		vector<GRBVar> sx(5);
		vector<GRBVar> sy(5);

		vector<vector<GRBVar>> P(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> E(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> F(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> U(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> V(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> B(rounds-1, vector<GRBVar>(64));
		vector<vector<GRBVar>> G(rounds-1, vector<GRBVar>(320));
		vector<vector<GRBVar>> H(rounds-1, vector<GRBVar>(320));

		vector<GRBVar> T1(320);
		vector<GRBVar> T2(320);

		for(r = 0 ; r<rounds; r++){
			for(i = 0; i<320; i++){
				X[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "X_"+itos(r)+"_"+itos(i));
				Y[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "Y_"+itos(r)+"_"+itos(i));
			}
			for(i = 0 ; i<64; i++){
				D[r][i]  = model.addVar(0, 1, 0, GRB_BINARY, "D_"+itos(r)+"_"+itos(i));
				w4[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "W4_"+itos(r)+"_"+itos(i));
				w44[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "W44_"+itos(r)+"_"+itos(i));
				w8[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "W8_"+itos(r)+"_"+itos(i));
				w88[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "W88_"+itos(r)+"_"+itos(i));
			}
		}

		for(r = 0 ; r<rounds-1; r++){
		    for(i = 0 ; i<64; i++){
		        P[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "P_"+itos(r)+"_"+itos(i));
		        E[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "E_"+itos(r)+"_"+itos(i));
		        F[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "F_"+itos(r)+"_"+itos(i));
		        U[r][i] = model.addVar(0, 3, 0, GRB_INTEGER, "U_"+itos(r)+"_"+itos(i));
		        V[r][i] = model.addVar(0, 2, 0, GRB_INTEGER, "V_"+itos(r)+"_"+itos(i));
		        B[r][i] = model.addVar(0, 2, 0, GRB_INTEGER, "B_"+itos(r)+"_"+itos(i));
		    }
		    for(i = 0 ; i<320; i++){
		        G[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "G_"+itos(r)+"_"+itos(i));
		        H[r][i] = model.addVar(0, 1, 0, GRB_BINARY, "H_"+itos(r)+"_"+itos(i));
		    }
		}

		for(r = 0; r<rounds-1; r++){

		    // Sbox constraints
		    for(i = 0 ; i<64; i++){
		        model.addConstr(X[r][i] + X[r][64 + i] + X[r][128 + i] + X[r][192 + i] + X[r][256 + i] >= D[r][i]);
				model.addConstr(X[r][i] + X[r][64 + i] + X[r][128 + i] + X[r][192 + i] + X[r][256 + i] <= 5*D[r][i]);
				model.addConstr(Y[r][i] + Y[r][64 + i] + Y[r][128 + i] + Y[r][192 + i] + Y[r][256 + i] >= D[r][i]);
				model.addConstr(Y[r][i] + Y[r][64 + i] + Y[r][128 + i] + Y[r][192 + i] + Y[r][256 + i] <= 5*D[r][i]);

				sx[0] = X[r][i] ;
				sx[1] = X[r][64 + i] ;
				sx[2] = X[r][128 + i] ;
				sx[3] = X[r][192 + i] ;
				sx[4] = X[r][256 + i] ;


				sy[0] = Y[r][i] ;
				sy[1] = Y[r][64 + i] ;
				sy[2] = Y[r][128 + i] ;
				sy[3] = Y[r][192 + i] ;
				sy[4] = Y[r][256 + i] ;

				model.addConstr(w4[r][i] + w44[r][i] + w8[r][i] + w88[r][i]== D[r][i]);
				sbox_w4(model, sx, sy, w4[r][i]);
				sbox_w44(model, sx, sy, w44[r][i]);
				sbox_w8(model, sx, sy, w8[r][i]);
				sbox_w88(model, sx, sy, w88[r][i]);

		    }

		    // rho_west [left shift]
		    for(i = 0; i<64;i++) T1[i] = Y[r][((i + EE[0]) % 64) + 0] ;
		    for(i = 0; i<64;i++) T1[64+i] = Y[r][((i + EE[1]) % 64) + 64] ;
		    for(i = 0; i<64;i++) T1[128+i] = Y[r][((i + EE[2]) % 64) + 128] ;
		    for(i = 0; i<64;i++) T1[192+i] = Y[r][((i + EE[3]) % 64) + 192] ;
		    for(i = 0; i<64;i++) T1[256+i] = Y[r][((i + EE[4]) % 64) + 256] ;

		    // P operation
		    for(i = 0; i<64; i++){
		        model.addConstr(T1[i] +  T1[64 + i] + T1[128 + i] + T1[192 + i] + T1[256 + i] + P[r][i] == 2*U[r][i]) ;
		    }

		    // F operation
		    for(i = 0; i<64; i++){
		        model.addConstr(P[r][i] + P[r][(64+i-SS) % 64] + F[r][i] == 2*V[r][i]) ;
		    }

		    // E operation
		    for(i = 0; i<64; i++){
		        model.addConstr(P[r][i] + P[r][(64+i-RR) % 64] + E[r][i] == 2*B[r][i]) ;
		    }

		    // Rotation by [t0, t1, t2, t3, t4]
		    for(i = 0; i<64;i++) T2[i] = F[r][((64 + i - TT[0]) % 64)] ;
		    for(i = 0; i<64;i++) T2[64+i] = F[r][((64 + i - TT[1]) % 64)] ;
		    for(i = 0; i<64;i++) T2[128+i] = F[r][((64 + i - TT[2]) % 64)] ;
		    for(i = 0; i<64;i++) T2[192+i] = F[r][((64 + i - TT[3]) % 64)] ;
		    for(i = 0; i<64;i++) T2[256+i] = F[r][((64 + i - TT[4]) % 64)] ;

		    // G_i = E + (F>> t_i)
		    for(j = 0; j<5;j++){
                for(i = 0; i<64; i++){
                    XOR2add(model, E[r][i], T2[64*j + i], G[r][64*j +i]) ;
                }
            }

		    for(j = 0; j<5;j++){
		        for(i = 0; i<64;i++){
		            XOR2add(model, T1[64*j +i], G[r][((64+i-UU) % 64) + 64*j], H[r][64*j +i]) ;
		        }
		    }

		    // rho_east [left shift]
		    for(i = 0; i<64;i++) model.addConstr(X[r+1][i] == H[r][(( i + WW[0]) % 64) + 0]) ;
		    for(i = 0; i<64;i++) model.addConstr(X[r+1][64+i] == H[r][(( i + WW[1]) % 64) + 64]) ;
		    for(i = 0; i<64;i++) model.addConstr(X[r+1][128+i] == H[r][(( i + WW[2]) % 64) + 128]) ;
		    for(i = 0; i<64;i++) model.addConstr(X[r+1][192+i] == H[r][(( i + WW[3]) % 64) + 192]) ;
		    for(i = 0; i<64;i++) model.addConstr(X[r+1][256+i] == H[r][((  i + WW[4]) % 64) + 256]) ;


		}

        // Last round without linear layer
        for(i = 0 ; i<64; i++){
            model.addConstr(X[r][i] + X[r][64 + i] + X[r][128 + i] + X[r][192 + i] + X[r][256 + i] >= D[r][i]);
		    model.addConstr(X[r][i] + X[r][64 + i] + X[r][128 + i] + X[r][192 + i] + X[r][256 + i] <= 5*D[r][i]);
		    model.addConstr(Y[r][i] + Y[r][64 + i] + Y[r][128 + i] + Y[r][192 + i] + Y[r][256 + i] >= D[r][i]);
		    model.addConstr(Y[r][i] + Y[r][64 + i] + Y[r][128 + i] + Y[r][192 + i] + Y[r][256 + i] <= 5*D[r][i]);

		    sx[0] = X[r][i] ;
		    sx[1] = X[r][64 + i] ;
		    sx[2] = X[r][128 + i] ;
		    sx[3] = X[r][192 + i] ;
		    sx[4] = X[r][256 + i] ;

		    sy[0] = Y[r][i] ;
		    sy[1] = Y[r][64 + i] ;
		    sy[2] = Y[r][128 + i] ;
		    sy[3] = Y[r][192 + i] ;
		    sy[4] = Y[r][256 + i] ;

		    model.addConstr(w4[r][i] + w44[r][i] + w8[r][i] + w88[r][i]== D[r][i]);
		    sbox_w4(model, sx, sy, w4[r][i]);
		    sbox_w44(model, sx, sy, w44[r][i]);
		    sbox_w8(model, sx, sy, w8[r][i]);
		    sbox_w88(model, sx, sy, w88[r][i]);

		}


		vector<GRBLinExpr> KS(rounds);
		GRBLinExpr KSS;
		int AS[rounds];
		for(j = 0; j<rounds; j++){
		    for(i = 0 ; i<64; i++){
		        KS[j] += D[j][i];
		    }
		}
		for(i = 0 ; i<rounds; i++){
		    KSS += KS[i];
		}


		vector<GRBLinExpr> WS(rounds);
		GRBLinExpr WSS;
		int W[rounds];

		// Correlation in each round
		for(i = 0 ; i<rounds; i++){
		    for(j = 0 ; j<64; j++){
		        WS[i] += 2*w4[i][j] + 2*w44[i][j] + 1*w8[i][j] +  1*w88[i][j];
		    }
		}
		// Total correlation
		for(i = 0 ; i<rounds; i++){
		    WSS += WS[i];
		}

		GRBLinExpr p_bits;
		for(i = 0 ; i<64; i++) {
		    p_bits += P[0][i];

		}
		//model.addConstr(p_bits == 0) ;
		model.addConstr(KS[0] >= 1) ;



		char inp[321] = "............................1...............................1..."
                        "...1.........................1.................................."
                        "...1.........................1.................................."
                        "............................................................1..."
                        "...........1....................1...........................1...";

        for(i = 0 ; i<320; i++){
            if(inp[i] == '1'){
                //X[0][i].set(GRB_DoubleAttr_Start, 1);
                model.addConstr(X[0][i]==1);
            }
            else{
                //X[0][i].set(GRB_DoubleAttr_Start, 0);
                model.addConstr(X[0][i]==0);
            }
        }

        model.setObjective(WSS, GRB_MINIMIZE);


        model.optimize();

        // Print the linear trail

        for(r= 0; r<rounds-1; r++){
            for (i = 0; i < 5; i++) {
                for(j = 0; j<64; j++){
                    if (round(X[r][64*i+j].get(GRB_DoubleAttr_Xn)) == 1) cout<<"1";
                    else cout<<".";
                }
                cout << endl ;
            }
            cout << "----------------------------------------------------------------"<<" $chi$"<<endl ;
            for (i = 0; i < 5; i++) {
                for(j = 0; j<64; j++){
                    if (round(Y[r][64*i+j].get(GRB_DoubleAttr_Xn)) == 1) cout<<"1";
                    else cout<<".";
                }
                cout << endl ;
            }
            cout << "----------------------------------------------------------------"<<" $rho_{east}$"<<endl ;
            for (i = 0; i < 5; i++) {
                for(j = 0; j<64; j++){
                    if (round(Y[r][((j+WW[i])% 64) + 64*i].get(GRB_DoubleAttr_Xn)) == 1) {
                        cout<<"1";
                    }
                    else cout<<".";
                }
                cout << endl ;
            }
            cout << "----------------------------------------------------------------"<<" $theta$"<<endl ;
            for (i = 0; i < 5; i++) {
                for(j = 0; j<64; j++){
                    if (round(H[r][64*i + j].get(GRB_DoubleAttr_Xn)) == 1) {
                        cout<<"1";
                    }
                    else cout<<".";
                }
                cout << endl ;
            }
            cout << "----------------------------------------------------------------"<<" $rho_{west}$"<<endl ;
        }

        for(i = 0; i<5; i++){
            for(j = 0 ; j<64; j++) {
                if (round(X[rounds-1][64*i+j].get(GRB_DoubleAttr_Xn)) == 1) cout<<"1";
                else cout<<".";
            }
            cout << endl;
        }
        cout << "----------------------------------------------------------------" <<endl ;
		for(i = 0 ; i<rounds; i++){
		    AS[i] = round(KS[i].getValue());
		}
		cout << "Configuration: ";
		for(i = 0 ; i<rounds; i++){
		    cout << AS[i] << " " ;
		}
		cout << endl ;


		for(i = 0 ; i<rounds; i++){
		    W[i] = round(WS[i].getValue());
		}
		cout << "Weights: ";
		for(i = 0 ; i<rounds; i++){
		    cout << W[i] << " " ;
		}


		cout << endl ;

		int obj = round(model.getObjective().getValue());
		cout << "Total correlation: " << obj ;
		cout << endl ;



		if (model.get(GRB_IntAttr_Status) == GRB_INFEASIBLE) {
			return -1;
		}
		else if ((model.get(GRB_IntAttr_Status) == GRB_OPTIMAL)) {
		    return 1 ;
		}
		else {
			//cout << model.get(GRB_IntAttr_Status) << endl;
			return -2;
		}
	}
	catch (GRBException e) {
		cerr << "Error code = " << e.getErrorCode() << endl;
		cerr << e.getMessage() << endl;
	}
	catch (...) {
		cerr << "Exception during optimization" << endl;
	}
	return -1;
}

int minimum_weight(int rounds, int threadNumber) {
    gaston_model(threadNumber, rounds);
    return 0;
 }

