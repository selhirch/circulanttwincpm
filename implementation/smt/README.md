In this folder we provide the SMT models to obtain a 3-round upper bound on the differential and linear weight of
Gaston. The models are given in SMTLIB2 format to give a flexibility of choosing SMT solver to solve it. The models
return a trail with bound strictly less than the specified value.

For the `differential_model.smt2`, the value is defined in line `1497`:
    
    (bvult ?x424 (_ bv106 64))))))))))))))))))))))))))))))))))))))))))

To search for a differential trail with weight less than, say 100, simply replace `bv106` to `bv100`. Similarly, for the
`linear_model.smt2`, the value is defined in line `2647`:
    
    (bvult ?x429 (_ bv34 64))))))))))))))))))))))))))))))))))))))))))


The description of the variables are as follows :

1) Internal states: indexed in 2-dimension with each index uses 2-digits integer (e.g. `0102` refers to the value at round `01` and row `02`).

    - `x : input to the chi`
    - `y : input to the rho-east`
    - `z : input to the theta`
    - `t : input to the rho-west`

2) Theta-related: indexed in 1-dimension with 2-digit integer representing the value at specific round. 

    - `p : the value of P`
    - `q : the value of Q`
    - `e : the value of E`

3) Weight-related: indexed in 2-dimension with each index uses 2-digits integer (e.g. `0102` refers to the value at round `01` and column `02`).

    - `w : weight variable`: 
